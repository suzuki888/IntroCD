# Projeto: 
Análise de casos de COVID em unidades de saúde ao redor de rotas de transporte público de Curitiba.

## Equipe: 
DataHoarders

## Descrição: 
Obter um mapa de calor de curitiba sobre os casos de covid em determinados bairros a partir de dados de hospitais e unidades de saúde e relacioná-lo com o mapa de rotas de transporte público, possivelmente obtendo um cálculo de risco de infecção para cada rota.

## Membros: 

Eduardo Takeshi Voltareli Suzuki, 1949764, suzuki888, BSI, UTFPR

Fernando Xavier De souza, 1656155, onandoxavier, BSI, UTFPR

